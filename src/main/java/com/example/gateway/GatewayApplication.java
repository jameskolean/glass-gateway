package com.example.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GatewayApplication {

	public static void main(final String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}

	@Bean
	public RouteLocator myRoutes(final RouteLocatorBuilder builder) {
		return builder.routes()
				.route(p -> p.path("/**")
						.filters(f -> f.addRequestParameter("apikey",
								"2bdfd5686527f02e0e7ab6c85b3726934fd05f7da01a0e68c56c2af32b015ca5"))
						.uri("http://3.230.127.133/"))
				.build();
	}
}
